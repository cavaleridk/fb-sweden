<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class PostsController extends Controller
{	
	/**
	 * Show index page for Posts by members
	 * Currently the same as members index page,..
	 *
	 */
    public function showIndex()
    {
    	$user = Auth::user();
        return view('logged_in', ['user' => $user]);
    }
}

<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Socialite;

class AuthController extends Controller
{
    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return Response
     */
    public function redirectToFacebook()
    {
        return Socialite::driver('facebook')->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return Response
     */
    public function handleFacebookCallback()
    {
        $facebook_user = Socialite::driver('facebook')->user();

        $facebook_user_id = $facebook_user->getId(); // unique facebook user id

	    $user = User::where('facebook_id', $facebook_user_id)->first();

	    // register (if no user)
	    if (!$user) {
	        $user = new User;
	        $user->facebook_id = $facebook_user_id;
	        $user->name = $facebook_user->getName();
	        $user->email = $facebook_user->getEmail();
	        $user->save();
	    }

	    // login
	    Auth::loginUsingId($user->id);
       	
       	// Redirect to /
    }
}